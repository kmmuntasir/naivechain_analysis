# Blockchain Performance Analysis

This is an analysis project for analyzing blockchain performance on different block size - implemented in PHP.

This project runs a sample blockchain in real time and stores no data (neither in storage, nor in DB), working with data which are generated randomly in real time.

This blockchain is not properly functioning and does not have all the features of a proper blockchain. It was made just in experimental purpose.


# Credits

This project makes use of -

NaiveChain Implemented in PHP by Abdurrahman Shofy Adianto [ https://azophy.github.io ]

Adapted from Lauri Hartikk's code [ https://github.com/lhartikk/naivechain ]

C++ MD5 implementation of nectar_moon [https://codereview.stackexchange.com/questions/163872/md5-implementation-in-c11]
	