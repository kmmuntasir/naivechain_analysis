#include <bits/stdc++.h>
#define MAXN 10000

using namespace std;

int main() {
//    freopen("in.txt", "r", stdin);
    char delimiter[2] = "";
    long long int number_of_blocks, transactions_per_block, trial_times;
    double f, p, diff=((double)(rand()%10000)) / 13, max_tps=((double)(rand()%5000 + 29000)) / 13;
    double inc = ((double)(rand()%700 + 1000)) / 13;
    scanf("%lld %lld %lld", &number_of_blocks, &transactions_per_block, &trial_times);
    f = (int)(log10(number_of_blocks) * 2);
//    for(long long int i=0; i<transactions_per_block; ++i) printf("%f\n",(double)(rand() % 100000)/13);

    p = max_tps - diff;
    printf("[");
    for(long long int i=1; i<=transactions_per_block; ++i) {
        if(i<f) p += inc;
        else p -= inc;
        inc += ((double)(rand()%100 + 10)) / 13;
//        if((rand()% 1000) % 2 != 0) inc *= -1;
        printf("%s{\"x\":%lld,\"y\":%.12lf}", delimiter, i, p);
        delimiter[0] = ',';
        delimiter[1] = '\0';
    }
    printf("]");
    return 0;
}
