class Block {
public:
    int index;
    std::string prev_hash, self_hash, data;

	Block(int idx, std::string previousHash, std::string block_data) {
		index 	    = idx;
		prev_hash   = previousHash;
		data 		= block_data;
		self_hash 	= calculateThisHash();
	}

	static std::string calculateHash(std::string previousHash, std::string block_data) {
		return md5(previousHash+block_data);
	}

	std::string calculateThisHash() {
		return calculateHash(prev_hash, data);
	}

	void display() {
        std::cout << index << " : " << prev_hash << " " << self_hash << " " << data << std::endl;
	}
};

class BlockChain {
public:
	std::vector <Block> chain;

	BlockChain () {
        chain.push_back(getGenesisBlock());
	}

	void display() {
        for(int i=0; i<chain.size(); ++i) {
            chain[i].display();
        }
	}

	Block fetch_block(int idx) {
        return chain[idx];
	}

	static Block getGenesisBlock() {
	    return Block(0, "0", "Genesis Block Data");
	}

	Block getLatestBlock() {
		return chain[chain.size()-1];
	}


	void addBlock(Block blk) {
		if (isValidNewBlock(blk)) {
			chain.push_back(blk);
		}
	}

	Block generateNextBlock(std::string block_data) {
	    Block previousBlock = getLatestBlock();
	    int nextIndex = previousBlock.index + 1;
	    return Block(nextIndex, previousBlock.self_hash, block_data);
	}

	bool isValidNewBlock (Block newBlock) {
        Block previousBlock = getLatestBlock();
	    if (previousBlock.index + 1 != newBlock.index) {
	        std::cout << "invalid index";
	        return false;
	    } else if (previousBlock.self_hash != newBlock.prev_hash) {
	        std::cout << "invalid previoushash";
	        return false;
	    } else if (newBlock.calculateThisHash() != newBlock.self_hash) {
	        std::cout << "invalid hash: " << newBlock.calculateThisHash() << " " << newBlock.self_hash;
	        return false;
	    }
	    return true;
	}
};
