#include <bits/stdc++.h>
#include "cpp_md5.h"
#include "blockchain.h"
#define MAXN 10000

using namespace std;

int main() {

//    freopen("in.txt", "r", stdin);

    long long int number_of_blocks, transactions_per_block, trial_number;
    double elapsed_time = 0.0, trial_times[MAXN], tps_arr[MAXN];
    BlockChain chain = BlockChain();

    cin >> number_of_blocks >> transactions_per_block >> trial_number;


    // ==============================

    for(long long int x=1; x <= transactions_per_block; ++x) {
        long long int trial_key = 0;
	    for(long long int i=0; i<trial_number; ++i) {
            string data = "";
		    for(i=0; i<x; ++i) {
                char c = (rand() % 26) + 65;
		    	data.push_back(c);
		    }

            const clock_t begin_time = clock();

            for(int i=0; i<number_of_blocks; ++i) {
                Block prev_block = chain.getLatestBlock();
                Block block = Block((prev_block.index)+1, prev_block.self_hash, data);
                chain.addBlock(block);
            }

            elapsed_time = (float)( clock () - begin_time );
            elapsed_time /=  CLOCKS_PER_SEC;

            trial_times[trial_key++] = elapsed_time;

//            printf("%.12lf", elapsed_time);

		}

		double avg_time = 0;
		for(int i=0; i<trial_key; ++i) avg_time += trial_times[i];
		avg_time /= trial_number;

		double avg_tps = (x * number_of_blocks)/avg_time;
		tps_arr[x] = avg_tps;
    }



//    chain.display();

    return 0;
}
