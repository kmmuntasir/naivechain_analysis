#include <bits/stdc++.h>
#include "cpp_md5.h"
#include "blockchain.h"
#define MAXN 10000

using namespace std;

int main() {

    BlockChain chain = BlockChain();
    int number_of_blocks = 500000;
    string data = "hello world";

    const clock_t begin_time = clock();


    for(int i=0; i<number_of_blocks; ++i) {
        Block prev_block = chain.getLatestBlock();
        Block block = Block((prev_block.index)+1, prev_block.self_hash, data);
        chain.addBlock(block);
    }

    double elapsed_time = (float)( clock () - begin_time );
    elapsed_time /=  CLOCKS_PER_SEC;

//    chain.display();

    printf("%.12lf", elapsed_time);

    return 0;
}
