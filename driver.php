<?php
	$result_array = array();

	$number_of_blocks = (isset($_GET['number_of_blocks']))?$_GET['number_of_blocks']:1;
	$transactions_per_block = (isset($_GET['transactions_per_block']))?$_GET['transactions_per_block']:1;
	$trial_number = (isset($_GET['trial_number']))?$_GET['trial_number']:1;
	$trial_times = array();
	$avg_time = 0;
	$avg_tps = 0;
	$handler = (isset($_GET['handler']))?$_GET['handler']:'php';

    ini_set('memory_limit', '-1');
    // ini_set('max_execution_time', 3000); //300 seconds = 5 minutes

    set_time_limit(0);

	if($handler=='php') {
		$_SESSION['handler'] = 'php';

	/* =======================Done with PHP =========================== */
    for($x=1; $x <= $transactions_per_block; ++$x) {
	    for($i=0; $i<$trial_number; ++$i) {
		    $transactions = array();
		    for($i=0; $i<$x; ++$i) {
		    	$trans = array();
		    	$trans['sender'] = rand();
		    	$trans['receiver'] = rand();
		    	array_push($transactions, $trans);
		    }
		    $transactions = json_encode($transactions);

		    

			$chain = new BlockChain;
			$start_time = microtime(true);
			for($i=0; $i<$number_of_blocks; ++$i) {
				$prev_block = $chain->getLatestBlock();
				$block = new Block($prev_block->index+1, $prev_block->hash, time(), $transactions);
				$chain->addBlock($block);
			}
			$end_time = microtime(true);
			// $elapsed_time = ($end_time - $start_time).'<br>';
			$elapsed_time = ($end_time - $start_time);
			array_push($trial_times, $elapsed_time);
		}

		$avg_time = 0;
		foreach($trial_times as $tr_time)$avg_time += $tr_time;
		$avg_time /= $trial_number;

		$avg_tps = ($x * $number_of_blocks)/$avg_time;


		//echo $x.' - '. $avg_tps.'<br>';
		array_push($result_array, array('x'=>$x, 'y'=>$avg_tps));
    }
	$result_array = json_encode($result_array);

    /* ================================================================ */

	} else {
		$_SESSION['handler'] = 'cpp';
    /* ====================== Done with C++ =========================== */
		$input_filename = "in.txt";
		$input = fopen($input_filename, "w");
	    fwrite($input, $number_of_blocks."\n".$transactions_per_block."\n".$trial_number);
	    fclose($input);
	    $output_filename = 'out.json';

	    $path = "/home/munna/coder/localhost/blockchain_analysis/";
	    // echo $path.'<br>';
	    $program_filename = "blockchain_driver";
	    $command = $path.$program_filename.' < '.$path.$input_filename.' > '.$path.$output_filename;

	    // $output = array();
	    exec($command);
	    $result_array = file_get_contents($output_filename);
	  //   foreach ($output as $key => $avg_tps) {
			// array_push($result_array, array('x'=>$key+1, 'y'=>(double)$avg_tps));
	  //   }
    /* ================================================================ */
	}






?>