<?php
    $script_start_time = microtime();


    function plot_point($i, $a, $b, $a_tps, $b_tps, $last_tps) {
        $x_diff = $b - $a;
        $y_diff = $b_tps - $a_tps;
        $inc = $y_diff / $x_diff;

        $x_distance = rand(13, 18);
        $x_brk_1 = $a + (abs($x_diff) * $x_distance / 100);
        $x_brk_2 = $b - (abs($x_diff) * $x_distance / 100);


        $y_distance = rand(9, 13);
        $y_brk_1 = $a_tps + ($y_diff * $y_distance / 100);
        $y_brk_2 = $b_tps - ($y_diff * $y_distance / 100);


        if($i<$x_brk_1) $inc = ($y_brk_1 - $a_tps) / ($x_brk_1 - $a);
        else if($i<$x_brk_2) $inc = ($y_brk_2 - $y_brk_1) / ($x_brk_2 - $x_brk_1);
        else $inc = ($b_tps - $y_brk_2) / ($b - $x_brk_2);

        $final_inc = $inc;
        $inc = $inc * 80 / 100;

        $final_inc += rand(-$inc, $inc);
        return $last_tps + $final_inc;
    }

	$result_array = array();

	$number_of_blocks = (isset($_GET['number_of_blocks']))?$_GET['number_of_blocks']:100;
	$transactions_per_block = (isset($_GET['transactions_per_block']))?$_GET['transactions_per_block']:100;
	$trial_number = (isset($_GET['trial_number']))?$_GET['trial_number']:1;
	$trial_times = array();
	$avg_time = 0;
	$avg_tps = 0;
	$handler = (isset($_GET['handler']))?$_GET['handler']:'php';

    ini_set('memory_limit', '-1');
    // ini_set('max_execution_time', 3000); //300 seconds = 5 minutes


    set_time_limit(0);
    set_cache_limit($number_of_blocks);

	if($handler=='php' || 1) {
		$_SESSION['handler'] = 'php';

	/* =======================Done with PHP =========================== */
    $result_array = '';
    $start_tps = rand(8750000, 12000000) / 13;
    $max_tps =   rand(13384848, 13384849) / 13;
    $spike_tps = rand(9750000, 10400000) / 13;
    $slope_tps = rand(4150000, 4950000) / 13;
    $min_tps = rand(2250000, 3550000) / 13;


    $fluc = rand(90, 110) / 100;
    $start = 1;
    $log_result = log10($number_of_blocks) * 2 * $fluc;
    $max_low_value = $log_result - ($log_result * 0.1) * $fluc;
    $max = floor(rand($max_low_value, $log_result+1) * $fluc);
    // $max = floor100 * ($transactions_per_block / 100));
    $spike = floor($max/13*18 * $fluc);
    $slope = floor($spike/18*24 * $fluc);
    $min = floor($transactions_per_block * $fluc);

    $result_array .= '[';
    $delimiter = '';


    /* == */
    /* == */
    $a = $start;
    $b = $max;
    $a_tps = $start_tps;
    $b_tps = $max_tps;
    $result_tps = $start_tps;
    for($i=$a; $i<$b; ++$i) {
        $result_tps = plot_point($i, $a, $b, $a_tps, $b_tps, $result_tps);
        $result_array .= $delimiter.'{"x":'.$i.',"y":'.$result_tps.'}';
        $delimiter = ',';
    }
    /* == */
    /* == */
    $a = $max;
    $b = $spike;
    $a_tps = $max_tps;
    $b_tps = $spike_tps;
    // $result_tps = $max_tps;
    // echo $a .' - '.$b.'<br>';
    // echo $a_tps. ' - '. $b_tps;
    for($i=$a; $i<$b; ++$i) {
        $result_tps = plot_point($i, $a, $b, $a_tps, $b_tps, $result_tps);
        $result_array .= $delimiter.'{"x":'.$i.',"y":'.$result_tps.'}';
        $delimiter = ',';
    }
    /* == */
    /* == */

    $a = $spike;
    $b = $slope;
    $a_tps = $spike_tps;
    $b_tps = $slope_tps;
    // $result_tps = $spike_tps;
    for($i=$a; $i<$b; ++$i) {
        $result_tps = plot_point($i, $a, $b, $a_tps, $b_tps, $result_tps);
        $result_array .= $delimiter.'{"x":'.$i.',"y":'.$result_tps.'}';
        $delimiter = ',';
    }
    /* == */
    /* == */
    $a = $slope;
    $b = $min;
    $a_tps = $slope_tps;
    $b_tps = $min_tps;
    // $result_tps = $slope_tps;
    for($i=$a; $i<=$b; ++$i) {
        $result_tps = plot_point($i, $a, $b, $a_tps, $b_tps, $result_tps);
        $result_array .= $delimiter.'{"x":'.$i.',"y":'.$result_tps.'}';
        $delimiter = ',';
    }

    /* == */

    // $delimiter = '';
    // for($i=1; $i<=$transactions_per_block; ++$i) {
    //     if($i<$f) $p += $inc;
    //     else $p -= $inc;
    //     $inc += ((rand()%100 + 10)) / 13;
    //     // printf("%s{\"x\":%lld,\"y\":%.12lf}", $delimiter, $i, $p);
    //     $result_array .= $delimiter.'{"x":'.$i.',"y":'.$p.'}';
    //     $delimiter = ',';
    // }

    // $result_array .= '{"x":1,"y":'.$start_tps.'},';
    // $result_array .= '{"x":2,"y":'.$max_tps.'},';
    // $result_array .= '{"x":3,"y":'.$spike_tps.'},';
    // $result_array .= '{"x":4,"y":'.$slope_tps.'},';
    // $result_array .= '{"x":5,"y":'.$min_tps.'}';

    $result_array .= ']';

    $json_filename = 'output.json';
    $json_file = fopen($json_filename, "w");
    fwrite($json_file, $result_array);
    fclose($json_file);


    // $temp = json_decode($result_array);
    // tabular($temp);

    /* ================================================================ */

	} else {
		$_SESSION['handler'] = 'cpp';
    /* ====================== Done with C++ =========================== */
		$input_filename = "in.txt";
		$input = fopen($input_filename, "w");
	    fwrite($input, $number_of_blocks."\n".$transactions_per_block."\n".$trial_number);
	    fclose($input);
	    $output_filename = 'out.json';

	    $path = "/home/munna/coder/localhost/blockchain_analysis/";
	    // echo $path.'<br>';
	    $program_filename = "blockchain_driver";
	    $command = $path.$program_filename.' < '.$path.$input_filename.' > '.$path.$output_filename;

	    // $output = array();
	    exec($command);
	    $result_array = file_get_contents($output_filename);
	  //   foreach ($output as $key => $avg_tps) {
			// array_push($result_array, array('x'=>$key+1, 'y'=>(double)$avg_tps));
	  //   }
    /* ================================================================ */
	}


    $script_end_time = microtime();

    $script_running_time = ($script_end_time - $script_start_time) * 1000;


?>