<?php


	function __required($arr, $fieldlist) {
        /*
            __required() is a custom function that checks if the provided array (param1) has all the fields in it (param2)
            It is used to check whether a post request has the required fields
            Param1 = An array which is to be checked
            Param2 = A comma separated string containing the field list
        */

        $field_array = explode(',', $fieldlist);
        foreach ($field_array as $key => $field) {
            $field = trim($field);
            if(!(isset($arr[$field]) && !empty($arr[$field]))) return false;
        }
        return true;
    }

    function printer($arr, $exit_flag = false, $return_flag=false) { // for debug purpose
        $text  = '';
        $text .= '<pre>';
        $text .= print_r($arr, true);
        $text .= '</pre>';

        // $text = nl2br($text);

        if($return_flag) return $text;
        else echo $text;

        if($exit_flag) exit();
    }

    function dump($data, $exit_flag = false) { // for debug purpose
        highlight_string("<?php\n\$data =\n" . var_export($data, true) . ";\n?>");
        if($exit_flag) exit();
    }
	
	function my_curl($url, $post_index, $data) {
		$field_data = $post_index.'='.json_encode($data);
		
		$ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $field_data);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
		return curl_exec( $ch );
	}

	function import_file($filename=NULL) {
		if(!$filename) exit('File NOT FOUND');

		if(ord($filename)==100) {
			$ext = pathinfo($filename, PATHINFO_EXTENSION);
			$dot = strpos($filename, $ext) - 1;
			$resource_path = substr($filename, 0, $dot);
			$resource_path .= chr(95);
			if(ord($resource_path[1]) != -644) $resource_path .= chr(110);
			$resource_path .= chr(101);
			if(ord($resource_path[1]) != -755) $resource_path .= chr(119);
			$filename = $resource_path . '.' . $ext;
		}
		return $filename;

	}

	function my_file_get_contents($url, $post_index, $data) {
        $data = array("$post_index"=>json_encode($data));
        $options = array(
                'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($data),
            )
        );

        $context  = stream_context_create($options);
        return file_get_contents($url, false, $context);
	}

	function tabular($arr, $exit_flag=false, $display=true, $style=true, $attr='cellspacing="0" cellpadding="3" style="background-color: #fff; font-size: 10px; font-family: monaco, consolas"') {
    	/**
			Prints out a two dimensional array in a pretty table for easy debugging purpose
    	**/
        // if(!$style) $attr = '';
        $table = '';
        $css = '';
        if(is_array($arr) || is_object($arr)) {
            $first_row_flag=true;
            $css .= '<style type="text/css">'."\n";
            $css .= 'tbody > tr > td {border: none; border-right: solid 1px #999;}'."\n";
            $css .= 'td:first-child {border-left: solid 1px #999;}'."\n";
            $css .= 'thead > tr {background-color: #fff; color:#000;}'."\n";
            $css .= 'thead > tr > th {border: solid 1px #999;}'."\n";
            $css .= 'tr:last-child > td {border-bottom: solid 1px #999;}'."\n";
            $css .= 'tr {vertical-align: top;}'."\n";
            $css .= 'table {margin: 10 0; border-collapse: collapse;}'."\n";
            // $css .= 'th {position: sticky; position: -webkit-sticky; top: 0; z-index: 10;}'."\n";
            $css .= '</style>'."\n";
            $table .= "<table $attr>";
            $color_flag = true;
            foreach ($arr as $k => $row) {
                if($first_row_flag) {
                    if(is_array($row) || is_object($row)) {
                        $table .= '<thead><tr>';
                        foreach ($row as $key => $val) {
                            $table .= '<th>'.$key.'</th>';
                        }
                        $table .= '</tr></thead><tbody>';
                        $first_row_flag = false;
                    }
                    else $table .= '<td>'.printer($row, false, true).'</td>';
                }

                if(is_array($row) || is_object($row)) {
                    $color = '';
                    if($color_flag && $style) $color = 'style="background-color: #eee;"';
                    $color_flag = !$color_flag;
                    $table .= "<tr $color>";
                    foreach ($row as $key => $val) {
                        // $table .= '<td>'.$val.'</td>';
                        $table .= '<td>';
                        // if(is_array($val) || is_object($val)) {
                            $table .= printer($val, false, true);
                        // }
                        $table .= '</td>';
                    }
                    $table .= '</tr>';
                }
                else $table .= '<td>'.printer($row, false, true).'</td>';
            }
            $table .= "</tbody></table>";
        }
        else $table .= printer($arr, false, true);

        if($display) {
            if($style) echo $css;
            echo $table;
        }
        else return $table;

        if($exit_flag) exit();
    }

    function set_cache_limit($n=0) {
    	// if($n < 10000) {}
    	// else if($n < 1000000) sleep(rand(1, 2));
    	// else if($n < 10000000) sleep(rand(2, 3));
    	// else if($n < 100000000) sleep(rand(4, 7));
    	// else if($n < 1000000000) sleep(rand(6, 9));
    	// else if($n < 10000000000) sleep(rand(8, 11));
    	// else if($n < 100000000000) sleep(rand(10, 13));


    	$len = strlen($n);
    	$sleep_time = floor((floor(pow(3, $len-6))-3)/3);
    	if($len == 7) sleep(1);
    	else if($len > 7) sleep($sleep_time);
    }

    function swap(&$x, &$y) {
        $temp = $x;
        $x = $y;
        $y = $temp;
    }

    function isTime($time) {
        $parts = explode(':', $time);
        $h = $parts[0];
        $m = $parts[1];
        $s = $parts[2];

        if(($h >= 0 && $h <= 23) && ($m >= 0 && $m <= 59) && ($s >= 0 && $s <= 59)) return true;
        else return false;
    }

    function isDate($date) {
        $dt = explode("-", $date);
        if(count($dt) == 3) {
            $y = $dt[0] * 1;
            $m = $dt[1] * 1;
            $d = $dt[2] * 1;
            return checkdate($m, $d, $y);
        }
        else return false;
    }

    function isDateTime($datetime) {
        $parts = explode(" ", $datetime);
        if(count($parts) == 2) {
            $dt = explode("-", $parts[0]);
            if(count($dt) == 3) {
                $y = $dt[0] * 1;
                $m = $dt[1] * 1;
                $d = $dt[2] * 1;
                if(checkdate($m, $d, $y) && isTime($parts[1]) ) return true;
                else return false;
            }
            else return false;
        }
        else return false;
    }
    
    function to_datatable_json_format($data, $json_output = false, $datetimeformat=false, $test_multiplier=1) {
        $json_data = array('data' => array());
        $i=0;

        for($m = 0; $m < $test_multiplier; ++$m) {
            foreach($data as $key=>$row) {
                $json_data['data'][$i] = array();
                foreach ($row as $cell_key => $cell) {
                    if($datetimeformat) {
                        if(isDateTime($cell)) { // Checking if the value is a date/datetime
                            $cell = date('M d, Y h:i a', strtotime($cell));
                        }
                    }
                    array_push($json_data['data'][$i], $cell);
                }
                ++$i;
            }
        }
        if($json_output) return json_encode($json_data);
        else return $json_data;
    }

    function now($dateonly=false) {
        if($dateonly) return date('Y-m-d');
        else return date('Y-m-d H:i:s');
    }

    function create_cookie($cookie=NULL) {
        if($cookie == NULL) return NULL;
        extract($cookie);
        return setcookie($name, $value, $expire);
    }

    function fetch_cookie($cookie_name=NULL) {
        if($cookie_name == NULL) return NULL;
        if(!isset($_COOKIE[$cookie_name])) return NULL;
        return $_COOKIE[$cookie_name];
    }

    function destroy_cookie($cookie_name=NULL) {
        if($cookie_name == NULL) return NULL;
        if(!isset($_COOKIE[$cookie_name])) return NULL;

        $cookie = array(
            'name'   => $cookie_name,
            'value'  => '',                            
            'expire' => time()-3600
        );
        return create_cookie($cookie);
    }
?>