<!-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> -->
<script src="canvasjs.min.js"></script>
<script>
    window.onload = function () {

        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: false,
            zoomEnabled: true,
            axisY :{
                includeZero:false
            },
            data: data  // random generator below
        });
        chart.render();

    }

    // var limit = 100;

    // var y = 0;
    var dataPoints = [];

    // for (var i = 0; i < limit; i += 1) {
    //     y += (Math.random() * 10 - 5);
    //     dataPoints.push({
    //         x: i - limit / 2,
    //         y: y                
    //     });
    // }

    dataPoints = <?php echo json_encode($result_array); ?>;

    var dataSeries = {
        type: "line",
        dataPoints: dataPoints
    };

    var data = [];
    data.push(dataSeries);
    console.log(data);           

</script>