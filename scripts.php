<!-- <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script> -->
<script src="jquery.min.js"></script>
<script src="canvasjs.min.js"></script>
<script>
    <?php if($_SESSION['handler'] == 'php') { ?>
        window.onload = function () {
            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: false,
                zoomEnabled: true,
                axisY :{
                    includeZero:false
                },
                data: data  // random generator below
            });
            chart.render();

        }
        var dataPoints = [];
        dataPoints = <?php echo $result_array; ?>;
        var dataSeries = {
            type: "line",
            dataPoints: dataPoints
        };
        var data = [];
        data.push(dataSeries);
        // console.log(data);
    <?php } else { ?>
        window.onload = function () {
            var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: false,
                zoomEnabled: true,
                axisY :{
                    includeZero:false
                },
                data: data  // random generator below
            });
            chart.render();
        }


        var dataPoints = [];
        dataPoints = <?php echo $result_array; ?>;

        var dataSeries = {
            type: "line",
            dataPoints: dataPoints
        };

        var data = [];
        data.push(dataSeries);
        // console.log(data);

    <?php } ?>    

</script>