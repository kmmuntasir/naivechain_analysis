<!DOCTYPE HTML>
<html>
<head>
    <title>NaiveChain Analysis</title>
    <link href="style.css" type="text/css" rel="stylesheet">
</head>
<body>

    <div id="chart" style="display: block;">
        <div id="curtain">
        </div>
        <div id="chartContainer">

        </div>
        <div class="clear"></div>
    </div>
    <h3 class="text-center">
        X AXIS: TPB (Transactions Per Block)&nbsp;&nbsp;&nbsp;<br>
        Y AXIS: TPS (Transactions Per Second)
    </h3>
    <h4 class="text-center">
        <i>log<sub>10</sub><?php echo '('.$number_of_blocks.')'; ?> x 2</i> = <?php echo log10($number_of_blocks) * 2; ?>;
    </h4>
    <!-- <h4 class="text-center">
        Script Running Time: <?php printf("%.6f", $script_running_time); ?> seconds
    </h4> -->
    <div class="clear"></div>
    <br>
    <form class="pull-right" action="index.php" method="get">
    <table>
        <!-- <tr>
            <td><label>Handler</label></td>
            <td>
                <select name="handler">
                    <option <?php if(isset($_GET['handler']) && $_GET['handler'] == 'php') echo 'selected'; ?> value="php">PHP</option>
                    <option <?php if(isset($_GET['handler']) && $_GET['handler'] == 'cpp') echo 'selected'; ?> value="cpp">C++</option>
                </select>
            </td>
        </tr> -->
        <tr>
            <td><label>Total Blocks</label></td>
            <td><input autofocus="" type="text" name="number_of_blocks" value="<?php echo $number_of_blocks; ?>" placeholder="Total Blocks"></td>
        </tr>
        <tr>
            <td><label>Transactions Per Block</label></td>
            <td><input type="text" name="transactions_per_block" value="<?php echo $transactions_per_block; ?>" placeholder="Transactions Per Block"></td>
        </tr>
        <tr>
            <td><label>Trial for Averaging</label></td>
            <td><input type="text" name="trial_number" value="<?php echo $trial_number; ?>" placeholder="Trial for Averaging"></td>
        </tr>
        <tr>
            <td><label>Average Time Elapsed</label></td>
            <td><?php echo $avg_time; ?></td>
        </tr>
        <tr>
            <td><label>Average TPS</label></td>
            <td><?php echo $avg_tps; ?></td>
        </tr>
    </table>
        <input type="submit" value="Submit" style="display: block"> 
    </form>

    <table class="pull-left" border="1" cellpadding="5" cellspacing="0" style="display: none;">
        <tr>
            <th>TPB</th>
            <th>TPS</th>
        </tr>
        <?php

        // foreach ($result_array as $key => $result) {
        //     echo '<tr><td>'.$result['x'].'</td><td class="tps_cell">';
        //     printf("%15.2f", $result['y']);
        //     echo '</td></tr>';
        // }
        ?>
    </table>


    <div class="pull-left" style="margin-left: 50px;">
        <?php // printer($result_array); ?>
    </div>

    <div class="clear"></div>

    <?php require('scripts.php'); ?>
</body>
</html>